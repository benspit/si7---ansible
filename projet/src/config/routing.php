<?php
function getPage($db) {
    $lesPages['accueil'] = "actionAccueil;0";
    $lesPages['ajouter'] = "actionAjouterMachine;0";
    $lesPages['ajouterCat'] = "actionAjouterCat;0";
    $lesPages['remove'] = "actionSupprimer;0";
    $lesPages['configurer'] = "actionConfigurer;0";
    $lesPages['maintenance'] = "actionMaintenance;0";
    
    if($db != null) {
        if(isset($_GET['page'])) {
            $page = $_GET['page']; 
        }
        else{
            $page = 'accueil';
        }
        if (!isset($lesPages[$page])) {
            $page = 'accueil'; 
        }
    }
    else {
        $page = 'maintenance';
    }
    
    $explose = explode(';',$lesPages[$page]);
    $role = $explose[1];
    if ($role != 0) {
        if(isset($_SESSION['login']) || isset($_SESSION['role'])) {
            if($role!=$_SESSION['role']) {
                $contenu = 'actionAccueil';  
            }
            else {
                $contenu = $explose[0]; 
            }
        }
        else {
            $contenu = 'actionAccueil';  
        }
    }
    else {
        $contenu = $explose[0];
    }
          return $contenu;
}
?>