<?php

function actionAccueil($twig, $db) {
    $info['nb_host'] = 0;
    $info['nb_cat'] = 0;
    $host = file("/etc/ansible/hosts");
    if ($host) {
        $cat_actu = preg_replace("#\[(.+)$\]#", '$1', $host[0]);
        foreach ($host as $i => $hosts) {
            if (trim($hosts) != '[all:vars]' && trim($hosts) != 'ansible_ssh_pass=btsinfo' && trim($hosts) != 'ansible_ssh_user=root' && trim($hosts) != '') {
                if (preg_match("#\[[a-z]{2,30}\]#", $hosts)) {
                    $cat_actu = preg_replace("#\[(.+)\]#", '$1', $hosts);
                    $host[$i] = "";
                    $list_cat[$cat_actu] = 1;
                    $info['nb_cat'] ++;
                } else {
                    $info['nb_host'] ++;
                    $host[$i] = array();
                    $host[$i]['name'] = substr($hosts, 0, -1);
                    $host[$i]['cat'] = $cat_actu;
                }
            }
        }
    }

    $action = new Action($db);
    $listeActions = $action->select();
    foreach ($listeActions as $i => $a) {
        $listeActions[$i]['time'] = timeago($listeActions[$i]['time']);
    }

    $info['nb_actions'] = $action->count();
    echo $twig->render('index.html.twig', array('info' => $info, 'hosts' => $host, 'listeActions' => $listeActions));
}

function actionAjouterCat($twig, $db) {
    $info = array();
    $host = file("/etc/ansible/hosts");
    if (isset($_POST['btAjouter'])) {
        $nb_lines = count($host);

        $fileContent = array_merge(
                array_slice($host, 0, $nb_lines), array_slice($host, $nb_lines + 1), array('', PHP_EOL), array('[' . $_POST['cat'] . ']', PHP_EOL)
        );
        if (file_put_contents("/etc/ansible/hosts", implode('', $fileContent))) {
            $info['message'] = "La catégorie a été ajouté.";
            $action = new Action($db);
            $action->insert("La catégorie <b>" . $_POST['cat'] . "</b> a été ajoutée.");
        }
    }

    echo $twig->render('ajouter-cat.html.twig', array('info' => $info));
}

function actionAjouterMachine($twig, $db) {
    $info = array();
    $host = file("/etc/ansible/hosts");
    if ($host) {
        foreach ($host as $i => $hosts) {
            if (preg_match("#\[[a-z]{2,15}\]#", $hosts)) {
                $list_cat[preg_replace("#\[(.+)\]#", '$1', $hosts)] = $i;
            }
        }
    }
    if (isset($_POST['btAjouter'])) {
        $nb_lines = count($host);

        $fileContent = array_merge(
                array_slice($host, 0, $_POST['line']), array($_POST['adresse'], PHP_EOL), array_slice($host, $_POST['line'], $nb_lines - 1)
        );
        if (file_put_contents("/etc/ansible/hosts", implode('', $fileContent))) {
            $info['message'] = "La machine a été ajouté.";
            $action = new Action($db);
            $action->insert("La machine <b>" . $_POST['adresse'] . "</b> a été ajoutée.");
            exec('/home/eleve/Ansible/sshkey.sh root@' . $_POST['adresse'], $out);
        }
    }
    echo $twig->render('ajouter.html.twig', array('info' => $info, 'cat' => $list_cat));
}

function actionMaintenance($twig) {
    echo $twig->render('maintenance.html.twig', array());
}

function actionConfigurer($twig, $db) {
    $out = array();
    if (isset($_GET['hosts']) && isset($_GET['type'])) {
        exec("ansible-playbook /home/eleve/Ansible/" . $_GET['type'] . ".yml --extra-vars 'host=" . $_GET['hosts'] . "'", $out);
        $action = new Action($db);
        $action->insert("Installation de type <b>" . $_GET['type'] . "</b> sur les machines <b>" . $_GET['hosts'] . "</b>.");
    }
    $info['nb_host'] = 0;
    $info['nb_cat'] = 0;
    $host = file("/etc/ansible/hosts");
    if ($host) {
        $i = 0;
        foreach ($host as $hosts) {
            if (trim($hosts) != '[all:vars]' && trim($hosts) != 'ansible_ssh_pass=btsinfo' && trim($hosts) != 'ansible_ssh_user=root' && trim($hosts) != '') {
                if (preg_match("#\[[a-z]{2,30}\]#", $hosts)) {
                    $cat_actu = preg_replace("#\[(.+)\]#", '$1', $hosts);
                    $categorie[$i] = $cat_actu;
                    $info['nb_cat'] ++;
                    $i++;
                }
            }
        }
    }
    echo $twig->render('configurer.html.twig', array('info' => $info, 'categorie' => $categorie, 'out' => $out));
}

function actionSupprimer($twig, $db) {
    $host = file("/etc/ansible/hosts");

    if (isset($_GET['host'])) {
        $line_min = null;
        foreach ($host as $i => $h) {
            echo $h;
            if (trim($h) == $_GET['host']) {
                $line_min = $i;
            }
            echo '<br />';
        }
        if ($line_min != null) {
            $nb_lines = count($host);
            $fileContent = array_merge(
                    array_slice($host, 0, $line_min), array_slice($host, $line_min + 1, $nb_lines - 1)
            );
            if (file_put_contents("/etc/ansible/hosts", implode('', $fileContent))) {
                $action = new Action($db);
                $action->insert("La machine <b>" . $_GET['host'] . "</b> a été supprimée.");
            }
        }
    }
}

?>
