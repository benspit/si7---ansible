<?php
class Action {
    private $db;
    private $select;
    private $count;
    private $insert;
    
    public function __construct($db) {
        $this->db = $db;
        $this->select = $db->prepare("SELECT * FROM historique ORDER BY time DESC LIMIT 10");
        $this->count = $db->prepare("SELECT COUNT(*) FROM historique ORDER BY time");
        $this->insert = $db->prepare("INSERT INTO historique VALUES('',:name,NOW())");
    }
    
    public function select() {
        $this->select->execute(array());
        return $this->select->fetchAll();
    }
    
    public function count() {
        $this->count->execute(array());
        return $this->count->fetchColumn();
    }
    
    public function insert($name) {
        $this->insert->execute(array(':name'=>$name));
        $r = true;
        if ($this->insert->errorCode()!=0){
             print_r($this->insert->errorInfo());  
             $r=false;
        }
        return $r;
    }
}
