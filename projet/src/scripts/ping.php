<?php
function pingAddress($ip) {
    
    exec('ping -c1 -w1 '.$ip, $outcome, $status);
    if(isset($outcome[3])) {
        preg_match('/([0-9]+)% packet loss/', $outcome[3], $arr);
    }
    if (!isset($outcome[3]) || isset($arr[1]) && $arr[1] == 100 ) { 
        echo '<span style="color: red">Hors ligne</span>'; 
        
    } else { 
        echo '<span style="color: green">En ligne</span>'; 
    }
    
}
if(isset($_GET['host'])) {
    pingAddress($_GET['host']);
}

function pingAll() {
    exec('ansible all -m ping', $out, $status);
    print_r($out);
}