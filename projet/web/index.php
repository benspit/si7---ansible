<?php
session_start();
if(!empty($_GET['phpinfo'])) {
    phpinfo();
}
/* initialisation des fichiers TWIG */
require_once '../src/lib/vendor/autoload.php';
require_once '../src/config/routing.php';
require_once '../src/models/_classes.php';
require_once '../src/controllers/_controllers.php';
require_once '../src/config/parametres.php';
require_once '../src/app/connexion.php';
require_once '../src/scripts/fonctions.php';
$db = connect($config);
$loader = new Twig_Loader_Filesystem('../src/views/');
$twig = new Twig_Environment($loader, array());
$twig->addGlobal('session', $_SESSION);
$contenu = getPage($db);
// Exécution de la fonction souhaitée
$contenu($twig, $db);
?>